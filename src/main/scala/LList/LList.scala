package LList

import scala.annotation.tailrec

//case class Node[T](current: T, next: Node[T] = null)

sealed trait LList[T]{
  def isEmpty:Boolean
  def length: Int
  def ::(head:T) : LList[T] // add to the front of the list: head is element to add to front of List
  def ++(list:LList[T]) : LList[T] // concatenate two LLists together
  def printElement: String
  def toString: String
  def contains(element:T): Boolean
  def map[A](f: T => A):LList[A]
  def flatMap[A](f: T => A): LList[A]

  def filter(f: T => Boolean):LList[T]
  def flatten : LList[T]

  def Type: String
}

class Empty[T] extends LList[T]{
  override def contains(element:T): Boolean = false
  override def isEmpty: Boolean = true

  override def length: Int = 0

  override def ::(head:T) : LList[T] = {
    new NonEmpty[T](head, this)
  }

  override def ++(list: LList[T]): LList[T] = list

  override def map[A](f: T => A):LList[A] = new Empty[A]

  override def printElement: String = ""

  override def toString: String = ""

  override def flatMap[A](f: T => A): LList[A] = new Empty[A]

  override def filter(f: T => Boolean): LList[T] = this

  override def flatten: LList[T] = this

  override def Type: String = "LList"
}

class NonEmpty[T](head: T, tail: LList[T]) extends LList[T] {

  override def isEmpty: Boolean = false

  override def length: Int = 1 + tail.length

  override def ::(element:T) : LList[T] = {
    new NonEmpty[T](element, this)
  }

  override def ++(otherList:LList[T]) : LList[T] = new NonEmpty(head, tail.++(otherList))
  def printElement:String = if (tail.isEmpty) head + "" else head + "," + tail.printElement

  override def contains(element:T): Boolean = if (element == head) true else (tail.contains(element ))
  override def map[A](f: T => A):LList[A] = new NonEmpty[A](f(head), tail.map(f))

  override def toString: String = s"[$printElement]"

  override def flatMap[A](f: T => A): LList[A] = new Empty[A]

  override def filter(f: T => Boolean): LList[T] = new Empty[T]

//  override def flatten : LList[T] = if (this.tail.isEmpty) new NonEmpty[T](this.head, this.tail) else new NonEmpty[T](this.head, this.tail.flatten)

  override def flatten : LList[T] = {
    println(s"Head: ${LList(this.head)}")
    if (this.isEmpty) new Empty[T]
    else if (this.head.getClass.getName.equals("LList.NonEmpty")) LList.toLList(this.head).flatten.++(this.tail.flatten)
    else this.tail.flatten.::(this.head)

  }


  override def Type: String = "LList"
}

object LList{
  def apply[T](elem: T*): LList[T] = {

    @tailrec
    def applyRec(elem:Seq[T], acc: LList[T] = new Empty[T]):LList[T] = {
      if (elem.isEmpty) acc
      else applyRec(elem.slice(0, elem.length-1), acc.::(elem.last))
    }

    applyRec(elem)
  }

  def toLList[T](tobeConverted: T): LList[T] = {
    println(s"${tobeConverted.getClass.getName}")
    if (tobeConverted.getClass.getName.equals("LList.NonEmpty")) tobeConverted.asInstanceOf[LList[T]] else apply(tobeConverted)
  }
}

/*

get the element before the tail
the make it the head of otherList in a new NonEmpty
Do this till head == Empty

 */