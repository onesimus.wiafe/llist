import LList.LList

object Main {

  def main(args: Array[String]): Unit = {
    val list = LList[String]()

    println(list)
    val newList = list.::("testing")
    println(newList)

    val newList1 = newList.::("testing1")
    println(newList1)


    //
    val elist = LList[Int](1,2,3)
    val flist = LList[Int](4, 5, 6)
    val glist = elist.++(flist)
    println(elist)
    println(glist)

    val flatList = LList(LList(1, 2, 4), LList(5, 6, 8))
    println(s"Flatlist: $flatList")

    println(flatList.flatten)

//    val x = List(List(1,2,3), List(1,2,3))
//    println(x.flatMap(_.map(y => y)))
//
//    def add(x:Int) = List(x+2)
//    val y = List(5, 6, 7,8)
//    val z = y.map(a => add(a))
//    println(z)
//    println(z.head.concat(z.tail))
//
//    println(x.flatten)

  }

}
